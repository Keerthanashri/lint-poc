var express = require('express');
var fs = require("fs");
var https = require("https");
const qs = require('qs');
var bodyParser = require("body-parser")
var path = require('path');
var jwt = require('jsonwebtoken')
var cors = require('cors');
const Auth = require('./auth');
const { default: axios } = require('axios');

var app = express();
const port = 3001
let tokenData , kid;

const key = fs.readFileSync('./certs/mysitename.key');
const cert = fs.readFileSync('./certs/mysitename.crt');
const options = {
    key: key,
    cert: cert,
    ca: [
        fs.readFileSync('./certs/cert.pem'),
        fs.readFileSync('./certs/key.pem')
    ]
};

app.use(cors())
app.use(express.urlencoded({ extended: false }))
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));


app.post('/', async(req, res) => {
    const idtoken = req.body.id_token;
    const scopes = `https://purl.imsglobal.org/spec/lti-ags/scope/lineitem 
    https://purl.imsglobal.org/spec/lti-ags/scope/lineitem.readonly 
    https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly 
    https://purl.imsglobal.org/spec/lti-ags/scope/score 
    https://purl.imsglobal.org/spec/lti-nrps/scope/contextmembership.readonly
    https://canvas.instructure.com/lti/public_jwk/scope/update
    https://canvas.instructure.com/lti/account_lookup/scope/show
    https://canvas.instructure.com/lti/data_services/scope/create
    https://canvas.instructure.com/lti/data_services/scope/show
    https://canvas.instructure.com/lti/data_services/scope/update
    https://canvas.instructure.com/lti/data_services/scope/list
    https://canvas.instructure.com/lti/data_services/scope/destroy
    https://canvas.instructure.com/lti/data_services/scope/list_event_types
    https://canvas.instructure.com/lti/feature_flags/scope/show
    https://canvas.instructure.com/lti-ags/progress/scope/show`;
    if (!idtoken) throw new Error('ID_TOKEN_INVALID');
    tokenData = idtoken
    let verifiedDetails = await Auth.validateToken(idtoken);
    kid = verifiedDetails.kid
    res.redirect(`https://crystaldelta.instructure.com/login/oauth2/auth?client_id=123670000000000365&response_type=code&redirect_uri=https://localhost:3001/token&state=xyzz&scope=${scopes}`);
});

app.get('/token' , async (req,res) => {
    try{
        console.log(req.query)
        const token = await axios({
            url : 'https://crystaldelta.instructure.com/login/oauth2/token',
            method : 'POST',
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded'
            },
            data : qs.stringify({
                grant_type : 'authorization_code',
                client_id : '123670000000000365',
                client_secret : 'VBXUt4iGoXZjoKpSr3EkWNZ7vX6vTXdInWA8OCeF8763Y4B8QwYPLRc1oi6LX0u5',
                redirect_uri : 'https://localhost:3001/',
                code : req.query.code
            })
        });
        res.redirect("/");
    } catch (err) {
        console.log(err);
    }
})


app.get('/postData' , async (req , res) => {
    try {
        const dataPayload = {
            "@context":"http://purl.imsglobal.org/ctx/lti/v1/ContentItem",
            "@graph":[
                {
                   "@id":"itemId",
                   "url":"https://www.youtube.com/embed/RyxiPBj0HTA",
                   "text":"Video file -&amp;hellip;",
                   "@type":"LtiLinkItem",
                   "title":"43bef340c0089a0c3c0147a093227fe4.200518_Lossy_12_IP_h264-23941.mov (00:20)",
                   "mediaType":"application\/vnd.ims.lti.v1.ltilink",
                   "windowTarget":"",
                   "placementAdvice":{
                      "displayWidth":400,
                      "presentationDocumentTarget":"iframe",
                      "displayHeight":285
                   }
                }
             ],
        }
        const request = await axios({
            url : `https://crystaldelta.instructure.com/courses/510/external_content/success/external_tool_dialog?content_items=%7B%22%40context%22%3A%22http%3A%5C%2F%5C%2Fpurl.imsglobal.org%5C%2Fctx%5C%2Flti%5C%2Fv1%5C%2FContentItem%22%2C%22%40graph%22%3A%5B%7B%22%40id%22%3A%22itemId%22%2C%22url%22%3A%22https%3A%5C%2F%5C%2F2467182.kaf.kaltura.com%5C%2Fbrowseandembed%5C%2Findex%5C%2Fmedia%5C%2Fentryid%5C%2F0_rkg7nba1%5C%2FshowDescription%5C%2Ffalse%5C%2FshowTitle%5C%2Ffalse%5C%2FshowTags%5C%2Ffalse%5C%2FshowDuration%5C%2Ffalse%5C%2FshowOwner%5C%2Ffalse%5C%2FshowUploadDate%5C%2Ffalse%5C%2FplayerSize%5C%2F400x285%5C%2FplayerSkin%5C%2F43675502%5C%2F%22%2C%22text%22%3A%22Video+file+-%26amp%3Bhellip%3B%22%2C%22%40type%22%3A%22LtiLinkItem%22%2C%22title%22%3A%2243bef340c0089a0c3c0147a093227fe4.200518_Lossy_12_IP_h264-23941.mov+%2800%3A20%29%22%2C%22mediaType%22%3A%22application%5C%2Fvnd.ims.lti.v1.ltilink%22%2C%22windowTarget%22%3A%22%22%2C%22placementAdvice%22%3A%7B%22displayWidth%22%3A400%2C%22presentationDocumentTarget%22%3A%22iframe%22%2C%22displayHeight%22%3A285%7D%7D%5D%7D&lti_version=LTI-1p3p0&lti_message_type=ContentItemSelection&data=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZWZhdWx0X2xhdW5jaF91cmwiOiJodHRwczovLzI0NjcxODIua2FmLmthbHR1cmEuY29tL2Jyb3dzZWFuZGVtYmVkL2luZGV4L2Jyb3dzZWFuZGVtYmVkIn0.Hk42tXnZmkqZ00Qt3A_JqQK3_yhqjN8XwLbIfZeyp_Q`,
            method : 'POST',
            headers : {
                'Content-Type' : 'x-www-form-urlencoded'
            },
            
        })
        res.send*request.data
    } catch (err) {
        console.log(err);
    }
})

app.post('/launch', function (req, res) {
    const state = encodeURIComponent([...Array(25)].map(_ => ((Math.random() * 36) | 0).toString(36)).join``);
    const nonce = encodeURIComponent([...Array(25)].map(_ => ((Math.random() * 36) | 0).toString(36)).join``);
    const redirectUrl = `https://canvas.instructure.com/api/lti/authorize_redirect?scope=openid&response_type=id_token&client_id=${req.body.client_id}&redirect_uri=${req.body.target_link_uri}&login_hint=${req.body.login_hint}&lti_message_hint=${req.body.lti_message_hint}&response_mode=form_post&nonce=${nonce}&prompt=none&state=${state}`;
    res.redirect(redirectUrl);
});



let server = https.createServer(options, app);
server.listen(port);
server.on('listening', () => {
    let addr = server.address();
    let bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    console.log('Listening on port', bind);
});

