const axios = require('axios');
const Jwk = require('rasha');
const jwt = require('jsonwebtoken');

// Authentication class manages RSA keys and validation of tokens.
class Auth {
  // Resolves a promisse if the token is valid following LTI 1.3 standards.
  async validateToken(token) {
    const decoded = jwt.decode(token, { complete: true });
    if (!decoded) throw new Error('INVALID_JWT_RECEIVED');
    
    console.log('Decoded data: ', decoded);

    const kid = decoded.header.kid;
    let validationParameters={ 
        alg: decoded.header.alg,
        maxAge: 200
    };

    const authConfig = {
        method: 'JWK_SET',
        key: 'https://canvas.instructure.com/api/lti/security/jwks',
        clientId: '123670000000000365',
    };

    switch (authConfig.method) {
      case 'JWK_SET': {
        console.log('Retrieving key from jwk_set');
        if (!kid) throw new Error('KID_NOT_FOUND');

        const keysEndpoint = authConfig.key;
        const res = await axios.get(keysEndpoint);
        console.log('Received key res:', res.data);
        const keyset = res.data.keys;
        console.log('Keyset:', keyset);
        if (!keyset) throw new Error('KEYSET_NOT_FOUND');
        const jwk = keyset.find(key => {
          return key.kid === kid;
        });
        if (!jwk) throw new Error('KEY_NOT_FOUND');
        console.log('Converting JWK key to PEM key');
        const key = await Jwk.export({ jwk: jwk });
        const verified = await this.verifyToken(token, key, validationParameters, authConfig);
 
        const verifiedUser = {
          email : verified.email,
          name : verified.name,
          clientId: verified.clientId,
          role: verified.userRole,
          userId: verified.userId,
          accountAdmin: verified.accountAdmin,
        }
        return {verifiedUser , kid};
      }
      default: {
        console.log('No auth configuration found for platform');
        throw new Error('AUTHCONFIG_NOT_FOUND');
      }
    }
  }

  // Verifies a token.
  async verifyToken(token, key, validationParameters, platform) {
    console.log('Attempting to verify JWT with the given key');
    const verified = jwt.verify(token, key, { algorithms: [validationParameters.alg] });
    await this.oidcValidation(verified, platform, validationParameters);
    const verifiedUser = await this.claimValidation(verified);
    verified.clientId = platform.clientId;
    verified.userRole = verifiedUser.roles;
    verified.userId = verifiedUser.user_id;
    verified.accountAdmin = verifiedUser.is_root_account_admin;
    return verified;
  }

  // Validates de token based on the OIDC specifications.
  async oidcValidation(token, platform, validationParameters) {
    console.log('Initiating OIDC aditional validation steps');
    const aud = this.validateAud(token, platform);
    const alg = this.validateAlg(validationParameters.alg);
    console.log('Token signature verified');
    return Promise.all([aud, alg ]);
  }

  // Validates Aud.
  validateAud(token, platform) {
    console.log("Validating if Audience claim matches the value of the tool's clientId given by the platform");
    console.log("Tool's clientId: " + platform.clientId);
    if (Array.isArray(token.aud)) {
      console.log('More than one aud listed, searching for azp claim');
      if (token.azp && token.azp !== platform.clientId) throw new Error('AZP_DOES_NOT_MATCH_CLIENTID');
    }
    return true;
  }

  // Validates Aug.
  async validateAlg(alg) {
    console.log('Checking alg claim. Alg: ' + alg);
    if (alg !== 'RS256') throw new Error('ALG_NOT_RS256');
    return true;
  }

  // Validates de token based on the LTI 1.3 core claims specifications.
  async claimValidation(token) {
    console.log('Initiating LTI 1.3 core claims validation');
    if (
      token['https://purl.imsglobal.org/spec/lti/claim/message_type'] !== 'LtiResourceLinkRequest' &&
      token['https://purl.imsglobal.org/spec/lti/claim/message_type'] !== 'LtiDeepLinkingRequest'
    )
      throw new Error('NO_MESSAGE_TYPE_CLAIM');

    if (token['https://purl.imsglobal.org/spec/lti/claim/message_type'] === 'LtiResourceLinkRequest') {
      console.log('Checking Target Link Uri');
      if (!token['https://purl.imsglobal.org/spec/lti/claim/target_link_uri']) throw new Error('NO_TARGET_LINK_URI_CLAIM');

      console.log('Checking Resource Link Id claim');
      if (
        !token['https://purl.imsglobal.org/spec/lti/claim/resource_link'] ||
        !token['https://purl.imsglobal.org/spec/lti/claim/resource_link'].id
      )
        throw new Error('NO_RESOURCE_LINK_ID_CLAIM');
    }

    console.log('Checking LTI Version claim');
    if (!token['https://purl.imsglobal.org/spec/lti/claim/version']) throw new Error('NO_LTI_VERSION_CLAIM');
    if (token['https://purl.imsglobal.org/spec/lti/claim/version'] !== '1.3.0') throw new Error('WRONG_LTI_VERSION_CLAIM');

    console.log('Checking Deployment Id claim');
    if (!token['https://purl.imsglobal.org/spec/lti/claim/deployment_id']) throw new Error('NO_DEPLOYMENT_ID_CLAIM');

    console.log('Checking Sub claim');
    if (!token.sub) throw new Error('NO_SUB_CLAIM');

    console.log('Checking Roles claim');
    if (!token['https://purl.imsglobal.org/spec/lti/claim/roles']) throw new Error('NO_ROLES_CLAIM');
    console.log('verification completed')
    return token['https://purl.imsglobal.org/spec/lti/claim/custom']
  }
}

module.exports = new Auth();